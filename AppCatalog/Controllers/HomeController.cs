﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;
using System.Security.Cryptography.X509Certificates;

using System.Security.Cryptography;
using Jose;

using AppCatalog.Config;


namespace AppCatalog.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
 
            // Cert created with http://www.cert-depot.com/

            String clientCertificateXml = "<RSAKeyValue><Modulus>zrEJ+n3ohp73g4OJUulLT3DuN/ey8vmBpdgaT1CCMkNsLXMkH7DMCfWg/XMS7FN7FeZpwDAvmiRkYiUSxKnVfDMWth4Y4u2TBFbBa7vq60GLsC4JdlXzNqpQOmmVuPsCkdpX0gHOAUyR/0SfXsjAGBDu4ntO0VELnEpCRrSYQt8=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            
            X509Certificate2 serverCert = Certificate.Get();
            X509Certificate2 clientCert = new X509Certificate2();
            RSACryptoServiceProvider cryptoProvider = new RSACryptoServiceProvider();
            cryptoProvider.FromXmlString(clientCertificateXml);

            String token = Request.QueryString["launch"];
            if (token != null)
            {
                RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider) serverCert.PrivateKey as RSACryptoServiceProvider;
                RSACryptoServiceProvider privateKey1 = new RSACryptoServiceProvider();
                privateKey1.ImportParameters(privateKey.ExportParameters(true));
                string jsonDecrypted = Jose.JWT.Decode(token, privateKey1);
                ViewBag.jsonDecrypted = jsonDecrypted;
            
                string jsonDecryptedDecoded = Jose.JWT.Decode(jsonDecrypted, cryptoProvider, JwsAlgorithm.RS256);
                ViewBag.jsonDecryptedDecoded = jsonDecryptedDecoded;

                Session["jsonDecryptedDecoded"] = jsonDecryptedDecoded;

                try {
                    String tokenWrongSignature = jsonDecrypted.Substring(0, jsonDecrypted.Length - 3) + "ABC";
                    Jose.JWT.Decode(tokenWrongSignature, cryptoProvider, JwsAlgorithm.RS256);
                } catch (Exception e) {
                    ViewBag.exception = e;
                }
            }

            ViewBag.PublicKey = serverCert.GetPublicKeyString();
            ViewBag.PublicKeyXML = serverCert.PublicKey.Key.ToXmlString(false);          
            ViewBag.Token = token;

            //String authURI = "https://adfs.fia.nhn.no/adfs/oauth2/authorize?client_id=db2d3cc9-d5ba-4300-9b9d-29f865d0ac70&redirect_uri=http%3A%2F%2Fehelselabapps.azurewebsites.net%2FHome%2FApps&response_type=token&resource=http%3A%2F%2Fapps.ehelselab.com%2Fvelferd%2Fapi%2F&scope=patient%2F%2A.%2A&nonce=6130755535249629&state=3%28%230%2F%21~";
            String authURI = "/Home/Apps";
            return Redirect(authURI);
        }

        public ActionResult jwk()
        {

            X509Certificate2 serverCert = Certificate.GetJSONCert();
            RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)serverCert.PrivateKey;
            RSACryptoServiceProvider serverCertCanSign = new RSACryptoServiceProvider();

            var key = privateKey.ExportParameters(false);

            var n = Base64Url.Encode(key.Modulus);
            var e = Base64Url.Encode(key.Exponent);

            String jwk = "{ \"keys\":[ { \"kty\": \"RSA\", \"alg\":\"RS256\", \"n\":\"" +n+ "\",   \"e\":\""+e+"\"}]}";

            return Content(jwk, "application/json");
        }

        public ActionResult Apps()
        {
            ViewBag.Token = Session["jsonDecryptedDecoded"];

            return View();
        }


        public ActionResult LaunchApp()
        {

            String uri = "http://apps.ehelselab.com/velferd/"; //Request.QueryString["redirect"];
            String appid = "velferd";
            String scope = "patient/*.*";
            String patientResourceId = "1-26";
            String practitionerId = "12345";

            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var now = Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);
            var payload = new Dictionary<string, object>()
            {
                { "exp", now },
                { "sub", practitionerId },
                { "scope", scope },
                { "iss", "https://oauth.ehelselab.com" },
                { "aud", "https://oauth.ehelselab.com/resources" },
                { "patient", patientResourceId },
                { "idp", "idsrv" }
            };

            X509Certificate2 serverCert = Certificate.GetJSONCert();
            RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)serverCert.PrivateKey;
            RSACryptoServiceProvider serverCertCanSign = new RSACryptoServiceProvider();
            serverCertCanSign.ImportParameters(privateKey.ExportParameters(true));
            string token = Jose.JWT.Encode(payload, serverCertCanSign, JwsAlgorithm.RS256);

            
            if (uri == null) {
                return View();
            } else {

                String redirectUri = uri+ "#access_token="+token+"&expires_in=3600&";
                return Redirect(redirectUri);
            }
        }


    }
}