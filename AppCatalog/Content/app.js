﻿

angular.module('catalogApp', [ ]).controller('CatalogController', function($scope) {
  
    $scope.appList = [
            {
                "name": "Avstandsoppfølging",
                "image": "/Images/dame.png",
                "description":"Her kan du se opplysninger som er rapportert inn fra pasienter som har tjenesten avstandsoppfølging. ",
                "url": "http://apps.ehelselab.com/velferd/"

            },
            {
                "name": "Helsenorge.no",
                "image": "/Images/helsenorge.png",
                "description": "Beskrivelse. Bla bla bla. ",
                "url": "http://apps.ehelselab.com/velferd/"

            },
            {
                "name": "Beslutningsstøtte",
                "image": "/Images/veivalg.png",
                "description": "Beskrivelse. Bla bla bla. ",
                "url": "http://apps.ehelselab.com/velferd/"

            },
            {
                "name": "Velferdsteknologi",
                "image": "/Images/vt.png",
                "description": "Veiviser og råd til velferdsteknologi.",
                "url": "http://apps.ehelselab.com/velferd/"

            }
    ];
});
